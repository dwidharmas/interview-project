import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import ProductView from "@/views/Product/ProductView.vue";
import ProductAdd from "@/views/Product/ProductAdd.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "product",
    component: ProductView,
  },
  {
    path: "/add",
    name: "product-add",
    component: ProductAdd,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
