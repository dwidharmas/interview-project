interface ProductRating {
  rate: number;
  count: number;
}

type ProductCategory =
  | "men's clothing"
  | "women's clothing"
  | "electronics"
  | "jewelery";

interface ProductFormInt {
  title: string;
  price: number;
  description: string;
  category: ProductCategory;
  image?: string;
}

interface Product {
  id: number;
  title: string;
  price: number;
  description: string;
  category: ProductCategory;
  image?: string;
  rating: ProductRating;
}

export { Product, ProductRating, ProductCategory, ProductFormInt };
