import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faStar,
  faPerson,
  faMale,
  faFemale,
  faDiamond,
  faTelevision,
  faThumbsUp,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faStar,
  faPerson,
  faMale,
  faFemale,
  faDiamond,
  faTelevision,
  faThumbsUp,
  faPlus
);
