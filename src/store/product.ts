import { defineStore } from "pinia";
import api from "@/api/fakeapi";
import { Product } from "@/interfaces/product";

export const useProductStore = defineStore("product", {
  state: () => ({
    products: [] as Product[],
    lastId: 0,
  }),
  getters: {
    getProducts(state) {
      return state.products;
    },
  },
  actions: {
    async fetchProducts() {
      const localProduct = localStorage.getItem("products");
      try {
        if (!localProduct) {
          const data = await api.get("/products");
          localStorage.setItem("products", JSON.stringify(data.data));
          this.products = data.data;
        } else {
          this.products = JSON.parse(localProduct);
          const length = this.products.length;
          this.lastId = this.products[length - 1].id;
        }
      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
    addProduct(payload: Product) {
      const products = [...this.products];
      products.push(payload);
      localStorage.setItem("products", JSON.stringify(products));
    },
  },
});
