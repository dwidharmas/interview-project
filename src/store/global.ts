import { defineStore } from "pinia";

export const useGlobalStore = defineStore("global", {
  state: () => ({
    isLoading: false,
  }),
  getters: {
    getIsLoading(state) {
      return state.isLoading;
    },
  },
  actions: {
    setIsLoading(status: boolean) {
      this.isLoading = status;
    },
  },
});
